import { DestinoViaje } from './destino-viaje-model';
import { Subject, BehaviorSubject } from 'rxjs';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './tarea-state-model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';


@Injectable()
export class TareasApiClient {
    tareas: DestinoViaje[] = [];
    constructor(private store: Store<AppState>,@Inject(forwardRef(()=>APP_CONFIG)) private config: AppConfig,
    private http: HttpClient) {
        this.store
        .select(state => state.tareas)
        .subscribe((data) =>{
            console.log('tareas sub store');
            console.log(data);
            this.tareas=data.items;
        });
        this.store
        .subscribe((data)=>{
            console.log('all store');
            console.log(data);
        })
	}
	add(d:DestinoViaje){
      const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
      const req = new HttpRequest('POST',this.config.apiEndpoint +'/my',{nuevo: d.tarea},{headers: headers});
      this.http.request(req).subscribe((data: HttpResponse<{}>)=>{
          if(data.status ===200){ 
            this.store.dispatch(new NuevoDestinoAction(d));
            const myDb = db;
            myDb.tareas.add(d);
            console.log('todos los destinos de la db!');
            myDb.tareas.toArray().then(tareas => console.log(tareas));
          }
      });

	}
    elegir(d: DestinoViaje){
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }
    getById(id: String): DestinoViaje {
        console.log("gola");
        return this.tareas.filter(function(d){return d.id.toString()===id;})[0];
      }
}