import { Component, OnInit ,Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje-model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction, VoteReinicioAction } from '../../models/tarea-state-model';
import { trigger, state, style, transition, animate} from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito',[
      state('estadoFavorito',style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito',[
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito',[
        animate('1s')
      ]),
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() tarea: DestinoViaje;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass="col-md-4";
  @Output() clicked: EventEmitter<DestinoViaje>;
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();    
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.tarea);
    return false;
  }
  voteUp(){
    this.store.dispatch(new VoteUpAction(this.tarea))
    return false;
  }
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.tarea))
    return false;
  }
  voteReinicio(){
    this.store.dispatch(new VoteReinicioAction(this.tarea))
    return false;
  }
}
