import { Component, OnInit, Output,EventEmitter, forwardRef, Inject } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje-model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged,switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-tarea',
  templateUrl: './form-tarea.component.html',
  styleUrls: ['./form-tarea.component.css']
})
export class FormTareaComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];
  constructor(fb: FormBuilder,@Inject(forwardRef(()=>APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      tarea: ['', Validators.compose([
        Validators.required,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      imagenUrl: [''],
      descripcion: ['']
     });
     this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio en el formulario',form);
    });
   };

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('tarea');
    fromEvent(elemNombre,'input').pipe(map((e:KeyboardEvent)=>(e.target as HTMLInputElement).value),
    filter(text =>text.length > 2),
    debounceTime(200),
    distinctUntilChanged(),
    switchMap((text: String)=> ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(AjaxResponse => this.searchResults = AjaxResponse.response);
  }
  guardar(tarea: string,imagenUrl: string,descripcion: string): boolean{
    const t = new DestinoViaje(tarea,imagenUrl,descripcion);
    this.onItemAdded.emit(t);
    return false;
  }


  nombreValidator(control: FormControl) :{[s: string]: boolean}{
    const longitud = control.value.toString().trim().length;
    if (longitud>0 && longitud<5){
      return { invalidNombre: true};
    }
    return null;
  }
  nombreValidatorParametrizable(minLong: number): ValidatorFn{
    return (control: FormControl):{ [s: string]: boolean} | null =>{
      const l = control.value.toString().trim().length;
      if (l>0 && l<minLong){
        return { minLongNombre: true};
      }
      return null;
    }
  }
}
