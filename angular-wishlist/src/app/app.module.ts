import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule , ActionReducerMap, Store} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormTareaComponent } from './components/form-tarea/form-tarea.component';
import { DestinosViajesState, reducerDestinosViajes, initializeDestinosViajesState, DestinosViajesEffects, InitMyDataAction} from './models/tarea-state-model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component'
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentsComponent } from './components/vuelos/vuelos-components/vuelos-components.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import Dexie from 'dexie';
import { DestinoViaje } from './models/destino-viaje-model';
import { TranslateService, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

// init routing
export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponentComponent },
  { path: 'mas-info', component: VuelosMasInfoComponentComponent },
  { path: ':id', component: VuelosDetalleComponent},
];



const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'tarea/:id', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'vuelos',
    component: VuelosComponentsComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childrenRoutesVuelos
  }
];

//app config
export interface AppConfig{
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
//fin app config




//redux init
export interface AppState{
  tareas: DestinosViajesState;
};

const reducers: ActionReducerMap<AppState> = {
   tareas: reducerDestinosViajes
};

const reducersInitialState={
  tareas: initializeDestinosViajesState()
}
//redux fin init

export function init_app(appLoadService: AppLoadService): () => Promise <any> {
  return () => appLoadService.initializeDestinosViajesState();
}
@Injectable()
class AppLoadService{
  constructor(private store: Store<AppState>, private http: HttpClient){}
  async initializeDestinosViajesState(): Promise<any>{
    const headers: HttpHeaders = new HttpHeaders ({'X-API-TOKEN':'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers: headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

//dexie db
/*export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string){}
}
*/
@Injectable({
  providedIn: 'root'
})
export class MyDataBase extends Dexie{
  tareas: Dexie.Table<DestinoViaje,number>;
  //translations: Dexie.Table<Translation, number>;
  constructor(){
    super('MyDatabase');
    this.version(1).stores({
      tareas: '++id,tarea,imagenUrl,descripcion',
    });
  /*  this.version(2).stores({
      tareas: '++id,tarea,imagenUrl,descripcion',
      translations: '++id,lang,key,value'
    });*/
  }
}

export const db = new MyDataBase();
//fin dexie db

// i18n ini
/*class TranslationLoader implements TranslateLoader{
  constructor(private htpp: HttpClient){}
  getTranslation(lang: string):Observable<any>{
    const promise = db.translations
                  .where('lang')
                  .equals(lang)
                  .toArray()
                  .then(results=>{
                    if(results.length === 0 ){
                      return this.http
                      .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang='+
                      lang)
                      .toPromise()
                      .then(apiResults =>{
                        db.translations.bulkAdd(apiResults);
                        return apiResults;
                      });
                    }
                    return results;
                  }).then((traducciones)=>{
                    console.log("traducciones cargadas:");
                    console.log(traducciones);
                    return traducciones;
                  }).then((traducciones)=>{
                    return traducciones.map((t)=>({[t.key]: t.value}));
                  });
      return from(promise).pipe(flatMap((elems)=>from(elems)));
  }*/
/*}
 function HttpLoaderFactory(http: HttpClient){
   return new TranslationLoader(http);
 }*/
//fin i18n


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormTareaComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentsComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers,{initialState: reducersInitialState,
    runtimeChecks:{
      strictStateImmutability: false,
      strictActionImmutability: false,
    }
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    NgxMapboxGLModule,
    BrowserAnimationsModule
   /* TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })*/
  ],
  providers: [AuthService,UsuarioLogueadoGuard,MyDataBase,{provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
  AppLoadService,{provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
